// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyASlGLN3U4Zsjw0VSind1JaN-MUl7Uq4I0",
    authDomain: "dks-clientpanel.firebaseapp.com",
    databaseURL: "https://dks-clientpanel.firebaseio.com",
    projectId: "dks-clientpanel",
    storageBucket: "dks-clientpanel.appspot.com",
    messagingSenderId: "694741529180"
  }
};
